#INSTALLATION
---
1. Ensure IIS 7 or 8 is running the on the machine

2. Inside IIS, setup a standard website:
	name: "CoolCompany"
	application pool: [CLR VERSION: 4.0 / Pipeline: Integrated]
  Use any local unused port. This tutorial uses port 89.
  Host name: can be anything. This tutorial uses (blank/empty)
  The "physical path" should point to: (directory you cloned)\code\Endpoints
  "Start Website Immediately" checkbox should be checked.

3. Open the solution \code\CoolCompany.sln in Visual Studio (this tutorial uses VS2017), open the Package Manager Console, and enter this command: Update-Package -reinstall    

4. After the process is finished downloading dependencies, build the solution. 

5. Browse to the URL of the new site. If hostname is blank, and port 89 was used, the URL is :
http://localhost:89/ 

This is the base URL of Api Calls, described below




#API CALLS
---

```javascript
GET ALL AGENTS
	[GET]
	/api/agent
 

CREATE AGENT
	[POST]
	/api/agent
	JSON BODY - Agent model (see below)
	
	
GET AGENT BY ID	
	[GET]
	/api/agent/(AGENT ID)  ex:  /api/agent/101
 

UPDATE AGENT
	[PUT]
	/api/agent/(AGENT ID)  ex:  /api/agent/101
	JSON BODY - Agent model (see below)


GET AGENT CUSTOMER LIST  (partial customer record)
 	[GET]
	/api/agent/(AGENT ID)/customer  ex:  /api/agent/101/customer
	

CREATE CUSTOMER FOR AGENT
 	[POST]
	/api/agent/(AGENT ID)/customer  ex:  /api/agent/101/customer
	JSON BODY - Customer model (see below)
 

DELETE CUSTOMER
	[DELETE]
	/api/agent/(AGENT ID)/customer/(customerId)   ex: /api/agent/101/customer/5054
	(returns boolean)
	
	
GET CUSTOMER BY ID (full customer record)
	[GET]
	/api/agent/(AGENT ID)/customer/(customerId)   ex: /api/agent/101/customer/5054
 

UPDATE CUSTOMER
	[PUT]
	/api/agent/(AGENT ID)/customer/(customerId)   ex: /api/agent/101/customer/5054
	JSON BODY - Customer model (see below)

```
 

#JSON MODEL EXAMPLES
---

##AGENT
```javascript
    { 
        "_id": 101, // NOT REQUIRED FOR CREATE
        "name": "Abe Simpson",
        "address": "2445 Onion Belt Ave",
        "city": "Springfield",
        "state": "IL",
        "zipCode": "62701",
        "tier": 1,
        "phone":
        {
            "primary": "217-345-2345",
            "mobile": "217-987-3211"
        }
    }
```




##CUSTOMER 
(full record / used for all request payloads )
```javascript
{
        "_id": 2354, // NOT REQUIRED FOR CREATE
        "agent_id": 101, // THIS VALUE IS OVERRIDEN BY 'AGENT ID' CONTEXT IN REQUEST URL
        "guid": "56e3aa2b-7c7d-438c-b1ca-7861eac7dcc3",
        "isActive": true,
        "balance": "$1,933.48",
        "age": 31,
        "eyeColor": "brown",
        "name": 
        {
            "first": "Marsha",
            "last": "George"
        },
        "company": "DOGNOSIS",
        "email": "marsha.george@dognosis.ca",
        "phone": "+1 (824) 479-2374",
        "address": "953 McKibbin Street, Coventry, Florida, 2823",
        "registered": "Friday, October 31, 2014 2:17 PM",
        "latitude": "70.299862",
        "longitude": "60.4194",
        "tags": 
        [
            "commodo",
            "ipsum",
            "excepteur",
            "officia",
            "officia"
        ]
    }
```



##CUSTOMER LIST ITEM 
(partial record / returned by server on certain requests)
```javascript
    {
        "_id": 8463,
        "agent_id": 321,
        "first": "Dina",
        "last": "Moreno",
        "city": "Yardville",
        "state": "Virgin Islands",
        "address": "378 Pershing Loop, Yardville, Virgin Islands, 6929"
    }
```

