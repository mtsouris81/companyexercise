﻿using CoolCompany.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoolCompany.Models
{
    public static class ManagerProvider
    {
        // A IOC Framework would do better life-time management, so this is a light version, instead of installing another framework

        static JsonAgentDataManager agentManager;
        static JsonCustomerDataManager customerManager;

        public static IAgentDataManager GetAgentManager()
        {
            if (agentManager == null)
            {
                agentManager = new JsonAgentDataManager(HttpContext.Current.Server.MapPath("~/App_Data/agents.json"));
            }
            return agentManager;
        }
        public static ICustomerDataManager GetCustomerManager()
        {
            if (customerManager == null)
            {
                customerManager = new JsonCustomerDataManager(HttpContext.Current.Server.MapPath("~/App_Data/customers.json"));
            }
            return customerManager;
        }
    }
}