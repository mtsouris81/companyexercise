﻿using CoolCompany.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CoolCompany.WebApp.Areas.Api.Controllers
{
    public class AgentController : ApiController
    {
        public IHttpActionResult Get()
        {
            return Get(0, int.MaxValue);
        }

        public IHttpActionResult Get(int skip, int take)
        {
            return Json(ManagerProvider.GetAgentManager().GetAll(skip, take));
        }

        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            var result = ManagerProvider.GetAgentManager().GetById(id);

            if (result == null)
                return NotFound();

            return Json(result);
        }

        public IHttpActionResult Post([FromBody]Agent value)
        {
            var result = ManagerProvider.GetAgentManager().Create(value);
            return Json(result);
        }

        public IHttpActionResult Put(int id, [FromBody]Agent value)
        {
            var manager = ManagerProvider.GetAgentManager();
            var agentCheck = manager.GetById(id);
            if (agentCheck == null)
                return NotFound();

            var result = manager.Update(value);
            return Json(result);
        }

    }
}
