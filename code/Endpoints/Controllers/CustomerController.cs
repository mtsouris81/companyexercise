﻿using CoolCompany.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CoolCompany.WebApp.Areas.Api.Controllers
{
    public class CustomerController : ApiController
    {
        public IHttpActionResult Get()
        {
            return Get(0, int.MaxValue);
        }

        public IHttpActionResult Get(int skip, int take)
        {
            return Json(ManagerProvider.GetCustomerManager().GetAll(skip, take));
        }

        [HttpGet]
        [Route("api/agent/{agentId}/customer/{customerId}")]
        public IHttpActionResult Get(int customerId)
        {
            var result = ManagerProvider.GetCustomerManager().GetById(customerId);

            if (result == null)
                return NotFound();

            return Json(result);
        }

        [HttpGet]
        [Route("api/agent/{agentId}/customer")]
        public IHttpActionResult ByAgent(int agentId)
        {
            var agentCheck = ManagerProvider.GetAgentManager().GetById(agentId);

            if (agentCheck == null)
                return NotFound();

            var result = ManagerProvider.GetCustomerManager().GetByAgentId(agentId);

            var converted = result.Select(x => new CustomerBasicInfo()
            {
                //Name – last, first – and city, state in List View)
                _id = x._id,
                agent_id = x.agent_id,
                first = x.name.first,
                last = x.name.last,
                city = AttemptToExtractCity(x.address).Trim(), 
                state = AttemptToExtractState(x.address).Trim(),
                address = x.address // if city / state extraction doesn't work, the consuming client can fall back to this generic 'address' field
            });
            return Json(converted);
        }

        private string AttemptToExtractCity(string address)
        {
            string[] parts = address.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length > 3)
            {
                return parts[1];
            }
            return "";
        }
        private string AttemptToExtractState(string address)
        {
            string[] parts = address.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length > 3)
            {
                return parts[2];
            }
            return "";
        }

        [Route("api/agent/{agentId}/customer")]
        public IHttpActionResult Post(int agentId, [FromBody]Customer value)
        {
            var agentCheck = ManagerProvider.GetAgentManager().GetById(agentId);

            if (agentCheck == null)
                return NotFound();

            value.agent_id = agentId;
            var result = ManagerProvider.GetCustomerManager().Create(value);
            return Json(result);
        }


        [Route("api/agent/{agentId}/customer/{customerId}")]
        public IHttpActionResult Put(int agentId, int customerId, [FromBody]Customer value)
        {
            var result = ManagerProvider.GetCustomerManager().Update(value);
            return Json(result);
        }

        [Route("api/agent/{agentId}/customer/{customerId}")]
        public IHttpActionResult Delete(int agentId, int customerId)
        {
            var result = ManagerProvider.GetCustomerManager().Delete(customerId);
            if (result == false) // false was returned because the record was not found
            {
                return NotFound();
            }
            return Json(result);
        }
    }
}
