﻿using System.Collections.Generic;

namespace CoolCompany.Models
{
    public class CustomerBasicInfo : IModelId
    {
        public int _id { get; set; }
        public int agent_id { get; set; }
        public string first { get; set; }
        public string last { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string address { get; set; }
    }
}
