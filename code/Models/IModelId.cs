﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolCompany.Models
{
    public interface IModelId
    {
        int _id { get; set; }
    }
}
