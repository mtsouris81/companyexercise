﻿namespace CoolCompany.Models
{
    public class PersonName
    {
        public string first { get; set; }
        public string last { get; set; }
    }
}
