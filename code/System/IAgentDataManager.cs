﻿using CoolCompany.Models;

namespace CoolCompany.Managers
{
    public interface IAgentDataManager : IDataManager<Agent>
    {
        // extend later
    }
}
