﻿using CoolCompany.Models;
using System.Collections.Generic;

namespace CoolCompany.Managers
{
    public interface ICustomerDataManager : IDataManager<Customer>
    {
        List<Customer> GetByAgentId(int id);
        bool Delete(int input);
    }
}
