﻿using CoolCompany.Models;
using System.Collections.Generic;

namespace CoolCompany.Managers
{
    public interface IDataManager<TModel>  where TModel : IModelId
    {
        List<TModel> GetAll(int skip, int take);
        TModel GetById(int id);
        TModel Create(TModel input);
        TModel Update(TModel input);
    }
}
