﻿using CoolCompany.Managers;
using CoolCompany.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoolCompany.Managers
{
    public class JsonAgentDataManager : JsonDataManager<Agent>, IAgentDataManager
    {

        static string AgentWriteLock = "agents";

        public JsonAgentDataManager(string path) : base(path)
        {

        }

        public override object GetLockObject()
        {
            return AgentWriteLock;
        }

        public Agent Create(Agent input)
        {
            if (input == null)
                throw new ArgumentException("Invalid input. Cannot be null.");

            ValidateForCreate(input);
            input._id = GetNextId();
            AddRecord(input);
            return input;
        }

        public Agent Update(Agent input)
        {
            if (input == null)
                throw new ArgumentException("Invalid input. Cannot be null.");

            ValidateForUpdate(input);
            var existingRecord = GetById(input._id);
            ApplyUpdates(input, existingRecord);
            Save(localRecordCache);
            return GetById(input._id); 
        }

        private void ApplyUpdates(Agent input, Agent existingRecord)
        {
            existingRecord.name = input.name;
            existingRecord.phone = input.phone;
            existingRecord.state = input.state;
            existingRecord.tier = input.tier;
            existingRecord.zipCode = input.zipCode;
            existingRecord.address = input.address;
            existingRecord.city = input.city;
        }


        public void ValidateForCreate(Agent input)
        {
            if (string.IsNullOrEmpty(input.name))
            {
                throw new ArgumentException("name is required.", "input");
            }
            if ((input.phone == null) || (string.IsNullOrEmpty(input.phone.mobile) && string.IsNullOrEmpty(input.phone.primary)))
            {
                throw new ArgumentException("phone cannot be null and must have at least one contact number (mobile or primary).", "input");
            }
        }

        public void ValidateForUpdate(Agent input)
        {
            if (input._id < 1)
            {
                throw new ArgumentException("invalid _id.", "input");
            }
            if (string.IsNullOrEmpty(input.name))
            {
                throw new ArgumentException("name is required.", "input");
            }
            if ((input.phone == null) || (string.IsNullOrEmpty(input.phone.mobile) && string.IsNullOrEmpty(input.phone.primary)))
            {
                throw new ArgumentException("phone cannot be null and must have at least one contact number (mobile or primary).", "input");
            }
        }
    }
}