﻿using CoolCompany.Managers;
using CoolCompany.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoolCompany.Managers
{
    public class JsonCustomerDataManager : JsonDataManager<Customer>, ICustomerDataManager
    {
        public JsonCustomerDataManager(string path) : base(path)
        {

        }

        public Customer Create(Customer input)
        {
            if (input == null)
                throw new ArgumentException("Invalid input. Cannot be null.");

            ValidateForCreate(input);
            input._id = GetNextId();
            AddRecord(input);
            return input;
        }

        public bool Delete(int id)
        {
            var itemToDelete = GetById(id);
            if (itemToDelete != null)
            {
                localRecordCache.Remove(itemToDelete);
            }
            else
            {
                return false;
            }
            Save(localRecordCache);
            return true;
        }

        public List<Customer> GetByAgentId(int id)
        {
            EnsureLocalRecordCache();
            return localRecordCache.Where(a => a.agent_id == id).ToList();
        }

        public Customer Update(Customer input)
        {
            if (input == null)
                throw new ArgumentException("Invalid input. Cannot be null.");

            ValidateForUpdate(input);

            var existingRecord = GetById(input._id);

            ApplyUpdates(input, existingRecord);

            Save(localRecordCache);

            return existingRecord;
        }

        private void ApplyUpdates(Customer input, Customer existingRecord)
        {
            existingRecord.address = input.address;
            existingRecord.name = input.name;
            existingRecord.age = input.age;
            existingRecord.balance = input.balance;
            existingRecord.company = input.company;
            existingRecord.email = input.email;
            existingRecord.eyeColor = input.eyeColor;
            existingRecord.guid = input.guid;
            existingRecord.isActive = input.isActive;
            existingRecord.latitude = input.latitude;
            existingRecord.longitude = input.longitude;
            existingRecord.name = input.name;
            existingRecord.phone = input.phone;
            existingRecord.registered = input.registered;
            existingRecord.tags = input.tags;
        }

        public void ValidateForCreate(Customer input)
        {
            if (input.name == null || string.IsNullOrEmpty(input.name.first) || string.IsNullOrEmpty(input.name.last))
            {
                throw new ArgumentException("name is required. both first and last name must be set.", "input");
            }
            if (string.IsNullOrEmpty(input.phone))
            {
                throw new ArgumentException("phone cannot be null or empty.", "input");
            }
        }

        public void ValidateForUpdate(Customer input)
        {
            if (input._id < 1)
            {
                throw new ArgumentException("invalid _id.", "input");
            }
            if (input.name == null || string.IsNullOrEmpty(input.name.first) || string.IsNullOrEmpty(input.name.last))
            {
                throw new ArgumentException("name is required. both first and last name must be set.", "input");
            }
            if (string.IsNullOrEmpty(input.phone))
            {
                throw new ArgumentException("phone cannot be null or empty.", "input");
            }
        }
    }
}