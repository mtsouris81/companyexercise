﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using CoolCompany.Models;

namespace CoolCompany.Managers
{
    public class JsonDataManager<TModel>  where TModel : IModelId
    {
        public virtual object GetLockObject()
        {
            return this;
        }

        protected List<TModel> localRecordCache;

        public string DataFilePath { get; private set; }

        public JsonDataManager(string path)
        {
            DataFilePath = path;
        }

        public TModel GetById(int id)
        {
            EnsureLocalRecordCache();
            return localRecordCache.Where(i => i._id == id).FirstOrDefault();
        }

        public List<TModel> GetAll(int skip, int take)
        {
            EnsureLocalRecordCache();
            return localRecordCache.Skip(skip).Take(take).ToList();
        }

        protected int GetNextId()
        {
            EnsureLocalRecordCache();
            int highestId = localRecordCache.OrderByDescending(a => a._id).Select(a => a._id).FirstOrDefault();
            highestId++;
            return highestId;
        }

        public void AddRecord(TModel record)
        {
            EnsureLocalRecordCache();
            localRecordCache.Add(record);
            Save(localRecordCache);
        }

        protected void Save(List<TModel> data)
        {
            var lockObject = GetLockObject();
            lock (lockObject)
            {
                try
                {
                    string json = JsonConvert.SerializeObject(data, Formatting.Indented);
                    string path = DataFilePath;
                    File.WriteAllText(path, json);
                }
                catch (Exception)
                {
                    Console.WriteLine("Failed data write");
                }
            }
        }

        protected void EnsureLocalRecordCache()
        {
            if (localRecordCache == null || localRecordCache.Count == 0)
            {
                if (string.IsNullOrEmpty(DataFilePath))
                {
                    throw new Exception("JsonDataManager DataFilePath cannot be null or empty");
                }
                try
                {
                    string json = File.ReadAllText(DataFilePath);
                    localRecordCache = JsonConvert.DeserializeObject<List<TModel>>(json);
                }
                catch
                {
                    Console.WriteLine("ERROR - unable to load " + DataFilePath);
                }
            }
        }
    }
}